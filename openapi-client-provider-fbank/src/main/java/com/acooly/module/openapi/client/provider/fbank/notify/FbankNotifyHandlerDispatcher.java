/**
 * coding by zhike
 */
package com.acooly.module.openapi.client.provider.fbank.notify;

import com.acooly.core.utils.Strings;
import com.acooly.module.openapi.client.api.ApiServiceClient;
import com.acooly.module.openapi.client.api.notify.AbstractSpringNotifyHandlerDispatcher;
import com.acooly.module.openapi.client.provider.fbank.FbankApiServiceClient;
import com.acooly.module.openapi.client.provider.fbank.OpenAPIClientFbankProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 富友 支付网关异步通知分发器
 *
 * @author liuyuxiang
 * @date 2016年5月12日
 */
@Component
public class FbankNotifyHandlerDispatcher extends AbstractSpringNotifyHandlerDispatcher {

    @Autowired
    private OpenAPIClientFbankProperties openAPIClientFbankProperties;

    @Resource(name = "fbankApiServiceClient")
    private FbankApiServiceClient apiServiceClient;

    @Override
    protected String getServiceKey(String notifyUrl, Map<String, String> notifyData) {
        return Strings.substringAfterLast(notifyUrl, openAPIClientFbankProperties.getNotifyUrlPrefix());
    }

    @Override
    protected ApiServiceClient getApiServiceClient() {
        return apiServiceClient;
    }
}
