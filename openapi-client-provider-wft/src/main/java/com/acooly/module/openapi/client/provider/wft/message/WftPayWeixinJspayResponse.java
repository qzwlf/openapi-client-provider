package com.acooly.module.openapi.client.provider.wft.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.wft.domain.WftApiMsgInfo;
import com.acooly.module.openapi.client.provider.wft.domain.WftResponse;
import com.acooly.module.openapi.client.provider.wft.enums.WftServiceEnum;
import com.acooly.module.openapi.client.provider.wft.support.WftAlias;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2017/11/28 21:10
 */
@Getter
@Setter
@WftApiMsgInfo(service = WftServiceEnum.PAY_WEIXIN_JSPAY, type = ApiMessageType.Response)
public class WftPayWeixinJspayResponse extends WftResponse {

    /**
     * --------------------------以下字段在 status 和 result_code 都为 0的时候有返回--------------------------------
     */

    /**
     * 动态口令，授权口令
     */
    @WftAlias("token_id")
    private String tokenId;

    /**
     * 原生态js支付信息或小程序支付信息，原生态js支付：is_raw为1时返回，json格式的字符串，作用于原生态js支付时的参数
     * 小程序支付：is_minipg为1时返回，json格式的字符串，作用于小程序支付时的参数
     */
    @WftAlias("pay_info")
    private String payInfo;
}
