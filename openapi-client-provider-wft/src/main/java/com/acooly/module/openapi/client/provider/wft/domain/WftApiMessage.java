/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhangpu
 * date:2016年3月31日
 *
 */
package com.acooly.module.openapi.client.provider.wft.domain;

import com.acooly.core.utils.ToString;
import com.acooly.module.openapi.client.api.message.ApiMessage;
import com.acooly.module.openapi.client.provider.wft.WftConstants;
import com.acooly.module.openapi.client.provider.wft.support.WftAlias;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

/** @author zhangpu */
@Getter
@Setter
public class WftApiMessage implements ApiMessage {

  /** 接口名称 */
  @WftAlias(value = "service")
  private String service;
  /** 服务版本 */
  @WftAlias(value = "version")
  private String version = "2.0";

  /** 字符集 */
  @WftAlias(value = "charset")
  private String charset = "UTF-8";

  /** 商户号 */
  @WftAlias(value = "mch_id")
  @NotBlank(message = "商户号不能为空")
  private String mchId;

  /** MD5签名结果 */
  @WftAlias(value = "sign", sign = false)
  private String sign;

  /** 签名类型 */
  @WftAlias(value = "sign_type")
  private String signType = WftConstants.SIGN_TYPE;

  /** 请求url */
  @WftAlias(value = "gatewayUrl", sign = false, request = false)
  private String gatewayUrl;

  public void doCheck() {}

  public String getSign() {
    return sign;
  }

  public void setSign(String sign) {
    this.sign = sign;
  }

  @Override
  public String toString() {
    return ToString.toString(this);
  }

  @Override
  public String getService() {
    return service;
  }

  @Override
  public String getPartner() {
    return mchId;
  }
}
