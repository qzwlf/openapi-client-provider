package com.acooly.module.openapi.client.provider.yipay.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.yipay.domain.YipayApiMsg;
import com.acooly.module.openapi.client.provider.yipay.domain.YipayResponse;
import com.acooly.module.openapi.client.provider.yipay.enums.YipayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/4/18 11:38
 */
@Getter
@Setter
@YipayApiMsg(service = YipayServiceNameEnum.PHONEINFO_VALID_ONLINE_STATUS,type = ApiMessageType.Response)
public class YipayPhoneInfoValidOnlineStatusResponse extends YipayResponse {


    /**
     * 1-正常在用
     * 2-停机
     * 3-在网但不可用
     * 4-不在网
     */
    private String stat;

    /**
     * 结果描述
     */
    private String desc;
}
