/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-16 16:11 创建
 */
package com.acooly.module.openapi.client.provider.cj;

import org.springframework.boot.context.properties.ConfigurationProperties;

import static com.acooly.module.openapi.client.provider.cj.CjProperties.PREFIX;


/**
 * @author zhangpu@acooly.cn
 */
@ConfigurationProperties(prefix = PREFIX)
public class CjProperties {
    public static final String PREFIX = "acooly.openapi.client.cj";

    /**
     * 网关地址
     */
    private String gatewayUrl;

    /**
     * 公钥
     */
    private String publicCertPath;

    /**
     * 私钥
     */
    private String privateCertPath;

    /**
     * 私钥密码
     */
    private String privateCertPassword;

    /**
     * 商户号
     */
    private String mertid;

    /**
     * 业务代码
     */
    private String businessCode;

    /**
     * 产品编码
     */
    private String productCode;

    /**
     * 企业账号
     */
    private String corpAccNo;

    /**
     * 本系统域名（用于自动生成回调地址）
     */
    private String domain;

    /**
     * 报文中签名字段名称
     */
    private String sign;

    /**
     * 连接超时时间（毫秒）
     */
    private long connTimeout = 10000;

    /**
     * 读超时时间（毫秒）
     */
    private long readTimeout = 30000;

    public String getGatewayUrl() {
        return gatewayUrl;
    }

    public void setGatewayUrl(String gatewayUrl) {
        this.gatewayUrl = gatewayUrl;
    }

    public String getPublicCertPath() {
        return publicCertPath;
    }

    public void setPublicCertPath(String publicCertPath) {
        this.publicCertPath = publicCertPath;
    }

    public String getPrivateCertPath() {
        return privateCertPath;
    }

    public void setPrivateCertPath(String privateCertPath) {
        this.privateCertPath = privateCertPath;
    }

    public String getPrivateCertPassword() {
        return privateCertPassword;
    }

    public void setPrivateCertPassword(String privateCertPassword) {
        this.privateCertPassword = privateCertPassword;
    }

    public String getMertid() {
        return mertid;
    }

    public void setMertid(String mertid) {
        this.mertid = mertid;
    }

    public String getBusinessCode() {
        return businessCode;
    }

    public void setBusinessCode(String businessCode) {
        this.businessCode = businessCode;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getCorpAccNo() {
        return corpAccNo;
    }

    public void setCorpAccNo(String corpAccNo) {
        this.corpAccNo = corpAccNo;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public long getConnTimeout() {
        return connTimeout;
    }

    public void setConnTimeout(long connTimeout) {
        this.connTimeout = connTimeout;
    }

    public long getReadTimeout() {
        return readTimeout;
    }

    public void setReadTimeout(long readTimeout) {
        this.readTimeout = readTimeout;
    }
}
