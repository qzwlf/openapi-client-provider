/*
 * www.acooly.cn Inc.
 * Copyright (c) 2018 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2018-02-10 02:53:54 创建
 */package com.acooly.module.openapi.client.provider.fudian.message.member;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fudian.domain.FudianApiMsg;
import com.acooly.module.openapi.client.provider.fudian.domain.FudianNotify;
import com.acooly.module.openapi.client.provider.fudian.enums.FudianServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author zhangpu 2018-02-10 02:53:54
 */
@Getter
@Setter
@FudianApiMsg(service = FudianServiceNameEnum.USER_CARD_BIND ,type = ApiMessageType.Notify)
public class UserCardBindNotify extends FudianNotify {

    /**
     * 账户号
     * 用户在本系统的唯一账户编号，由本系统生成.
     */
    @NotEmpty
    @Length(max=50)
    private String accountNo;

    /**
     * 绑卡银行
     * 绑卡接口开户银行
     */
    @NotEmpty
    @Length(max=256)
    private String bank;

    /**
     * 绑定银行卡号
     * 绑定的银行卡号，可用于提现
     */
    @NotEmpty
    @Length(max=32)
    private String bankAccountNo;

    /**
     * 银行对应编码
     * 绑卡银行对应的编码，见附录【4.4】
     */
    @NotEmpty
    @Length(max=32)
    private String bankCode;

    /**
     * 绑卡状态
     * 0-绑卡中，1-绑卡成功，2-绑卡失败
     */
    @NotEmpty
    @Length(max=2)
    private String status;

    /**
     * 验卡金额
     * 预留字段，暂不可用。
     */
    @NotEmpty
    @Length(max=32)
    private String deductMoney;


    /**
     * 商户号
     * 用于校验主体参数和业务参数一致性，保证参数的安全传输
     */
    @NotEmpty
    @Length(max=8)
    private String merchantNo;

    /**
     * 用户名
     * 用户在本系统的唯一标识，由本系统生成
     */
    @NotEmpty
    @Length(max=32)
    private String userName;
}