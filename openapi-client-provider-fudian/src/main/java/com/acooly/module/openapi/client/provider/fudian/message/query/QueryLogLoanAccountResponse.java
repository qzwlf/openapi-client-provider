/*
 * www.acooly.cn Inc.
 * Copyright (c) 2018 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2018-02-22 04:32:16 创建
 */
package com.acooly.module.openapi.client.provider.fudian.message.query;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fudian.domain.FudianApiMsg;
import com.acooly.module.openapi.client.provider.fudian.domain.FudianResponse;
import com.acooly.module.openapi.client.provider.fudian.enums.FudianServiceNameEnum;
import com.acooly.module.openapi.client.provider.fudian.message.query.dto.LoanAccountLog;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.List;

/**
 * @author zhangpu 2018-02-22 04:32:16
 */
@Getter
@Setter
@FudianApiMsg(service = FudianServiceNameEnum.QUERY_LOGLOANACCOUNT, type = ApiMessageType.Response)
public class QueryLogLoanAccountResponse extends FudianResponse {

    /**
     * 标的账户号
     * 商户标的账户号，由存管系统生成并确保唯一性
     */
    @NotEmpty
    @Length(max = 32)
    private String loanAccNo;

    /**
     * 资金流水列表
     * json格式："loanAccountLogList：[{“”:””}
     */
    @NotEmpty
    @Length(max = 5000)
    private List<LoanAccountLog> loanAccountLogList;

    /**
     * 标的号
     * 标的号，由存管系统生成并确保唯一性.
     */
    @NotEmpty
    @Length(max = 32)
    private String loanTxNo;

    /**
     * 商户号
     * 用于校验主体参数和业务参数一致性，保证参数的安全传输
     */
    @NotEmpty
    @Length(max = 8)
    private String merchantNo;
}