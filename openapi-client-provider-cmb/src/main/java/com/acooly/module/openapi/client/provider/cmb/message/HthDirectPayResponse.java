package com.acooly.module.openapi.client.provider.cmb.message;


import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.cmb.domain.CmbApiMsgInfo;
import com.acooly.module.openapi.client.provider.cmb.domain.CmbResponse;
import com.acooly.module.openapi.client.provider.cmb.enums.CmbServiceEnum;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@CmbApiMsgInfo(service = CmbServiceEnum.cmbDirectPay, type = ApiMessageType.Response)
public class HthDirectPayResponse extends CmbResponse {

}
