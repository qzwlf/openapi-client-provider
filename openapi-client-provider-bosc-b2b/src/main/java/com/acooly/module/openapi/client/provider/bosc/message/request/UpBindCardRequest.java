package com.acooly.module.openapi.client.provider.bosc.message.request;

import org.hibernate.validator.constraints.NotBlank;

import lombok.Getter;
import lombok.Setter;

import com.acooly.module.openapi.client.provider.bosc.domain.BoscRequestDomain;

@Getter
@Setter
public class UpBindCardRequest extends BoscRequestDomain {

	/**
	 * 操作员ID（需要修改信息的操作员ID（复核员））
	 */
	@NotBlank
	private String operatorId;

	/**
	 * 绑定卡清算行行号
	 */
	@NotBlank
	private String acctBank;

	/**
	 * 绑定银行对公账号
	 */
	@NotBlank
	private String account;

	/**
	 * 银行发送到复核员手机上的动态验证码
	 */
	@NotBlank
	private String dynamicCode;

}
