/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhangpu 
 * date:2016年3月31日
 *
 */
package com.acooly.module.openapi.client.provider.hx.domain;

import com.acooly.core.utils.ToString;
import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.message.ApiMessage;
import com.acooly.module.openapi.client.provider.hx.HxConstants;
import com.thoughtworks.xstream.annotations.XStreamOmitField;


/**
 * @author
 */
public class HxApiMessage implements ApiMessage {

    /**
     * webService paramName
     */
    @XStreamOmitField
    private String paramName;

    /**
     * webServic namespaceUrl
     */
    @XStreamOmitField
    private String namespaceUrl;

    /**
     * webServic localPart
     */
    @XStreamOmitField
    private String localPart;

    /**
     * 接口名称
     */
    @XStreamOmitField
    private String serviceCode;
    /**
     * 商户号
     */
    @XStreamOmitField
    private String partnerId;

    /**
     * 签名类型
     */
    @XStreamOmitField
    private String sign_type = HxConstants.SIGN_TYPE;

    /**
     * 请求url
     */
    @ApiItem(sign = false)
    @XStreamOmitField
    private String gatewayUrl;

    public void doCheck() {

    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    @Override
    public String getService() {
        return serviceCode;
    }

    @Override
    public String getPartner() {
        return partnerId;
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }

    public String getGatewayUrl() {
        return gatewayUrl;
    }

    public void setGatewayUrl(String gatewayUrl) {
        this.gatewayUrl = gatewayUrl;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public String getSign_type() {
        return sign_type;
    }

    public void setSign_type(String sign_type) {
        this.sign_type = sign_type;
    }

    public String getNamespaceUrl() {
        return namespaceUrl;
    }

    public void setNamespaceUrl(String namespaceUrl) {
        this.namespaceUrl = namespaceUrl;
    }

    public String getLocalPart() {
        return localPart;
    }

    public void setLocalPart(String localPart) {
        this.localPart = localPart;
    }

    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }
}
