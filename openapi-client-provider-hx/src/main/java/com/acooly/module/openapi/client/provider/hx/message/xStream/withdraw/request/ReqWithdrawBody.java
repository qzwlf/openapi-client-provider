package com.acooly.module.openapi.client.provider.hx.message.xStream.withdraw.request;


import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import lombok.Data;

@Data
@XmlAccessorType(XmlAccessType.FIELD)
@XStreamAlias("Body")
public class ReqWithdrawBody {

    /**
     *下发类型
     *1.B2C 个人银行账号下发
     *3.B2B 公司银行账号下发
     */
    @XStreamAlias("BizId")
    private String bizId;
    /**
     *下发渠道_下发渠道 1、即时渠道；2、快速渠道；3、普通
     *渠道
     */
    @XStreamAlias("ChannelId")
    private String channelId;
    /**
     *156：人民币
     */
    @XStreamAlias("Currency")
    private String currency;
    /**
     *格式：yyyyMMddHHmmss，例如：20160913120000
     */
    @XStreamAlias("Date")
    private String date;
    /**
     *这里的长度是指最大支持 25 个汉字
     */
    @XStreamAlias("Attach")
    private String attach;
    /**
     *此为 3DES 加密后的结果，加密前信息见下“ 下发明细列表
     *Detail”表
     *其中 3DES 加密的填充方式为：
     *DES/CBC/PKCS5Padding
     */
    @XStreamAlias("IssuedDetails")
    private ReqWithdrawIssued issuedDetails;

}
