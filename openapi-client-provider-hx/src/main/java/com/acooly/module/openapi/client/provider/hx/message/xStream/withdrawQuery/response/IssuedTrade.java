package com.acooly.module.openapi.client.provider.hx.message.xStream.withdrawQuery.response;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import lombok.Data;

/**
 * @author fufeng 2018/3/2 10:05.
 */
@Data
@XmlAccessorType(XmlAccessType.FIELD)
@XStreamAlias("IssuedTrade")
public class IssuedTrade {

    /**
     * 批次流水号
     */
    @XStreamAlias("BatchNO")
    private String batchNO;

    /**
     * IPS 订单号
     */
    @XStreamAlias("IpsBillNo")
    private String ipsBillNo;

    /**
     *商户订单号
     */
    @XStreamAlias("MerBillNo")
    private String merBillNo;

    /**
     *F2，例如 1.00
     */
    @XStreamAlias("TrdAmt")
    private String trdAmt;
    /**
     *F2，例如 1.00
     */
    @XStreamAlias("TrdInCash")
    private String trdInCash;


    /**
     *F2，例如 1.00
     */
    @XStreamAlias("TrdOutCash")
    private String trdOutCash;


    /**
     *订单状态_订单状态
     *默认：0
     *处理中：8
     *成功：10
     *失败：9
     */
    @XStreamAlias("OrdStatus")
    private String ordStatus;

    /**
     *出款状态
     *0：正常 4：退票
     *其中“退票“是指银行返回的最终结
     *果，一般最长为 T+3 工作日，具体以
     *银行反馈的时间为准.
     */
    @XStreamAlias("TrdStatus")
    private String trdStatus;

    /**
     *错误原因
     */
    @XStreamAlias("ErrorMsg")
    private String errorMsg;

    /**
     * 交易完成时间
     *yyyyMMddHHmmss
     */
    @XStreamAlias("TrdDoTime")
    private String trdDoTime;

}
