/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhangpu 
 * date:2016年4月5日
 *
 */
package com.acooly.module.openapi.client.provider.fuiou.message;

import com.acooly.module.openapi.client.provider.fuiou.domain.FuiouResponse;
import com.acooly.module.openapi.client.provider.fuiou.support.FuiouAlias;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * 网银充值 响应报文
 * 
 * @author zhangpu
 */
public class FuiouEbankDepositResponse extends FuiouResponse {

	/** 单位：分 */
	@Size(min = 1, max = 10)
	@FuiouAlias
	private String amt;

	@NotEmpty
	@Size(min = 1, max = 60)
	@FuiouAlias("login_id")
	private String loginId;

	@Size(min = 1, max = 60)
	@FuiouAlias
	private String rem;

	public String getAmt() {
		return amt;
	}

	public void setAmt(String amt) {
		this.amt = amt;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getRem() {
		return rem;
	}

	public void setRem(String rem) {
		this.rem = rem;
	}

}
