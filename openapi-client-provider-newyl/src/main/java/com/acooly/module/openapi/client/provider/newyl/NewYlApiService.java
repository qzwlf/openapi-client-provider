/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-25 14:40 创建
 */
package com.acooly.module.openapi.client.provider.newyl;


import com.acooly.core.utils.Strings;
import com.acooly.module.openapi.client.provider.newyl.enums.NewYl;
import com.acooly.module.openapi.client.provider.newyl.enums.NewYlServiceEnum;
import com.acooly.module.openapi.client.provider.newyl.message.*;
import com.acooly.module.openapi.client.provider.newyl.message.xStream.common.ReqInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author zhangpu 2017-09-25 14:40
 */
@Service
public class NewYlApiService {
	
	@Resource(name = "newYlApiServiceClient")
	private NewYlApiServiceClient newYlApiServiceClient;

	@Autowired
	private NewYlProperties newYlProperties;
	
	/**
	 * 单笔代扣
	 * @param request
	 * @return
	 */
	public NewYlRealDeductResponse newYlRealDeduct(NewYlRealDeductRequest request) {
		request.getReqInfo().setTrxCode(NewYl.TRX_CODE_交易代码_实时);
		request.getReqInfo().setVersion(NewYl.VERSION_版本);
		request.getReqInfo().setDataType(NewYl.DATA_TYPE_数据格式_XML);
		request.getReqInfo().setLevel(NewYl.LEVEL_处理级别_0);
		request.setServiceCode(NewYlServiceEnum.NEW_YL_REAL_DEDUCT.getCode());
		beforeRequest(request.getReqInfo());
		String merchantId = request.getRdReqBody().getTransSum().getMerchantId();
		if(Strings.isBlank(merchantId)) {
			request.getRdReqBody().getTransSum().setMerchantId(newYlProperties.getPartnerId());
		}
		return (NewYlRealDeductResponse) newYlApiServiceClient.execute(request);
	}

	/**
	 * 批量代扣
	 * @param request
	 * @return
	 */
	public NewYlBatchDeductResponse newYlBatchDeduct(NewYlBatchDeductRequest request) {
		request.getReqInfo().setTrxCode(NewYl.TRX_CODE_交易代码_批量);
		request.getReqInfo().setVersion(NewYl.VERSION_版本);
		request.getReqInfo().setDataType(NewYl.DATA_TYPE_数据格式_XML);
		request.getReqInfo().setLevel(NewYl.LEVEL_处理级别_5);
		request.setServiceCode(NewYlServiceEnum.NEW_YL_BATCH_DEDUCT.getCode());
		beforeRequest(request.getReqInfo());
		String merchantId = request.getBdReqBody().getTransSum().getMerchantId();
		if(Strings.isBlank(merchantId)) {
			request.getBdReqBody().getTransSum().setMerchantId(newYlProperties.getPartnerId());
		}
		return (NewYlBatchDeductResponse) newYlApiServiceClient.execute(request);
	}

	/**
	 * 代扣查询
	 * @param request
	 * @return
	 */
	public NewYlDeductQueryResponse newYlDeductQuery(NewYlDeductQueryRequest request) {
		request.getReqInfo().setTrxCode(NewYl.TRX_CODE_交易代码_结果查询);
		request.getReqInfo().setVersion(NewYl.VERSION_版本);
		request.getReqInfo().setDataType(NewYl.DATA_TYPE_数据格式_XML);
		request.getReqInfo().setLevel(NewYl.LEVEL_处理级别_0);
		request.setServiceCode(NewYlServiceEnum.NEW_YL_DEDUCT_QUERY.getCode());
		beforeRequest(request.getReqInfo());
		String merchantId = request.getQdReqBody().getQueryTrans().getMerchantId();
		if(Strings.isBlank(merchantId)) {
			request.getQdReqBody().getQueryTrans().setMerchantId(newYlProperties.getPartnerId());
		}
		return (NewYlDeductQueryResponse) newYlApiServiceClient.execute(request);
	}

	/**
	 * 提前设置公共信息
	 * @param reqInfo
	 */
	public void beforeRequest(ReqInfo reqInfo) {
		if(Strings.isBlank(reqInfo.getUserName())) {
			reqInfo.setUserName(newYlProperties.getUserName());
		}
		if(Strings.isBlank(reqInfo.getUserPass())) {
			reqInfo.setUserPass(newYlProperties.getUserPass());
		}

	}
}

