/**
 * create by zhangpu
 * date:2015年3月2日
 */
package com.acooly.module.openapi.client.provider.newyl;

import com.acooly.core.utils.Strings;
import com.acooly.core.utils.net.HttpResult;
import com.acooly.module.openapi.client.api.AbstractApiServiceClient;
import com.acooly.module.openapi.client.api.exception.ApiClientException;
import com.acooly.module.openapi.client.api.exception.ApiClientSocketTimeoutException;
import com.acooly.module.openapi.client.api.exception.ApiServerException;
import com.acooly.module.openapi.client.api.marshal.ApiMarshal;
import com.acooly.module.openapi.client.api.marshal.ApiUnmarshal;
import com.acooly.module.openapi.client.api.transport.Transport;
import com.acooly.module.openapi.client.provider.newyl.domain.NewYlNotify;
import com.acooly.module.openapi.client.provider.newyl.domain.NewYlRequest;
import com.acooly.module.openapi.client.provider.newyl.domain.NewYlResponse;
import com.acooly.module.openapi.client.provider.newyl.exception.ApiClientYlProcessingException;
import com.acooly.module.openapi.client.provider.newyl.marshall.NewYlNotifyUnmarshall;
import com.acooly.module.openapi.client.provider.newyl.marshall.NewYlRedirectMarshall;
import com.acooly.module.openapi.client.provider.newyl.marshall.NewYlRedirectPostMarshall;
import com.acooly.module.openapi.client.provider.newyl.marshall.NewYlRequestMarshall;
import com.acooly.module.openapi.client.provider.newyl.marshall.NewYlResponseUnmarshall;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.net.SocketTimeoutException;
import java.util.Map;

import javax.annotation.Resource;

import lombok.extern.slf4j.Slf4j;

/**
 * 上海银行P2P存管ApiService执行器  外部业务禁止使用
 *
 * @author zhangpu
 */
@Slf4j
@Component("newYlApiServiceClient")
public class NewYlApiServiceClient
        extends AbstractApiServiceClient<NewYlRequest, NewYlResponse, NewYlNotify, NewYlNotify> {

    public static final String PROVIDER_NAME = "newyl";

    @Resource(name = "newYlHttpTransport")
    private Transport transport;
    @Resource(name = "newYlRequestMarshall")
    private NewYlRequestMarshall requestMarshal;
    @Resource(name = "newYlRedirectMarshall")
    private NewYlRedirectMarshall redirectMarshal;
    @Resource(name = "newYlRedirectPostMarshall")
    private NewYlRedirectPostMarshall newYlRedirectPostMarshall;

    @Autowired
    private NewYlResponseUnmarshall responseUnmarshal;

    @Autowired
    private NewYlNotifyUnmarshall notifyUnmarshal;

    @Autowired
    private NewYlProperties newYlProperties;

    @Override
    public NewYlResponse execute(NewYlRequest request) {
        try {
            beforeExecute(request);
            if (Strings.isBlank(request.getGatewayUrl())) {
                request.setGatewayUrl(newYlProperties.getGatewayUrl());
            }
            String url = request.getGatewayUrl();
            String requestMessage = getRequestMarshal().marshal(request);
            log.info("请求报文:{}", requestMessage);
            String responseMessage = "";
            //对帐文件单独处理
            HttpResult httpResult = transport.request(requestMessage, url);
            responseMessage = httpResult.getBody();
            if (Strings.isBlank(responseMessage)) {
                throw new ApiClientException("响应报文位空");
            }
            NewYlResponse t = getResponseUnmarshal().unmarshal(responseMessage, request.getService());
            afterExecute(t);
            return t;
        } catch (ApiServerException ose) {
            log.error("服务器:" + ose.getMessage(), ose);
            throw ose;
        } catch (ApiClientException oce) {
            log.error("客户端:" + oce.getMessage(), oce);
            if (oce.getCause() != null && oce.getCause() instanceof SocketTimeoutException) {
                throw new ApiClientSocketTimeoutException("等待响应超时:" + oce.getMessage());
            }
            throw oce;
        } catch (ApiClientYlProcessingException acype) {
            throw acype;
        } catch (Exception e) {
            log.error("内部错误:" + e.getMessage(), e);
            throw new ApiClientException("内部错误:" + e.getMessage());
        }
    }

    @Override
    protected ApiMarshal<String, NewYlRequest> getRequestMarshal() {
        return this.requestMarshal;
    }

    @Override
    protected ApiUnmarshal<NewYlResponse, String> getResponseUnmarshal() {
        return this.responseUnmarshal;
    }

    @Override
    protected ApiUnmarshal<NewYlNotify, Map<String, String>> getNoticeUnmarshal() {
        return this.notifyUnmarshal;
    }

    @Override
    protected ApiMarshal<String, NewYlRequest> getRedirectMarshal() {
        return this.redirectMarshal;
    }

    @Override
    protected Transport getTransport() {
        return this.transport;
    }

    @Override
    protected ApiUnmarshal<NewYlNotify, Map<String, String>> getReturnUnmarshal() {
        return this.notifyUnmarshal;
    }

    @Override
    public String getName() {
        return PROVIDER_NAME;
    }


}
