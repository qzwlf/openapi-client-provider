/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年3月31日
 *
 */
package com.acooly.module.openapi.client.provider.baofu.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

/**
 * Fuiou 响应报文基类
 *
 * @author zhike
 */
@Getter
@Setter
public class BaoFuResponse extends BaoFuApiMessage {

    /**
     * 返回状态码
     */
    @XStreamAlias("resp_code")
    private String respCode;

    /**
     * 返回信息
     */
    @XStreamAlias("resp_msg")
    private String respMsg;

    /**
     * 订单发送时间
     */
    @XStreamAlias("trade_date")
    private String tradeDate;

    /**
     * 商户订单号
     * 唯一订单号，8-50 位字母和数字，
     * 注：支付请求中trans_id作为主键，如果有重复的交易请求存在，以第一个成功交易为准，后续重复交易不被受理。
     * 不支持商户提交重复订单号，对重复订单号将直接提示订单已经提交。
     */
    @XStreamAlias("trans_id")
    private String transId;

    /**
     * 宝付交易号 由宝付返回，用于在后续类交易中唯一标识一笔交易
     */
    @XStreamAlias("trans_no")
    private String transNo;
}
