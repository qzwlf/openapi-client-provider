package com.acooly.module.openapi.client.provider.yibao.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.yibao.domain.YibaoAlias;
import com.acooly.module.openapi.client.provider.yibao.domain.YibaoApiMsg;
import com.acooly.module.openapi.client.provider.yibao.domain.YibaoResponse;
import com.acooly.module.openapi.client.provider.yibao.enums.YibaoServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

/**
 * @author zhike 2018/6/26 14:37
 */
@Getter
@Setter
@YibaoApiMsg(service = YibaoServiceNameEnum.YIBAO_BINDPAY_REQUEST,type = ApiMessageType.Response)
public class YibaoSmsBindPayApplyResponse extends YibaoResponse {

    /**
     * 银行流水号
     */
    @YibaoAlias(value = "yborderid")
    private String bankOrderNo;

    /**
     * 计费商编
     */
    @Size(max = 32)
    @YibaoAlias(value = "csmerchantno")
    private String csMerchantNo;

    /**
     * 短验码
     * 商户发短验时返回的短验码
     */
    @Size(max = 32)
    @YibaoAlias(value = "smscode")
    private String smscode;

    /**
     * 短验发送方
     * YEEPAY：易宝
     * CUSTOMER：商户
     * BANK：银行
     */
    @Size(max = 32)
    @YibaoAlias(value = "codesender")
    private String codeSender;

    /**
     * 实际短验发送类型
     * VOICE：语音
     * MESSAGE：短信
     */
    @Size(max = 32)
    @YibaoAlias(value = "smstype")
    private String smsType;

    /**
     * 手机号
     * 空值，央行规定禁止返回敏感信息
     */
    @NotBlank
    @Size(max = 32)
    @YibaoAlias(value = "phone")
    private String mobileNo;

    /**
     * 订单状态
     * TO_VALIDATE：待短验确认
     * PAY_FAIL：支付失败
     * FAIL：系统异常
     * （FAIL 是非终态是异常状态，出现此状态建议查
     * 询）P
     */
    @YibaoAlias(value = "status")
    private String status;

    /**
     * 扩展字段
     */
    @Size(max = 42)
    @YibaoAlias(value = "free1")
    private String freeOne;

    /**
     * 扩展字段
     */
    @Size(max = 42)
    @YibaoAlias(value = "free2")
    private String freeTwo;

    /**
     * 扩展字段
     */
    @Size(max = 42)
    @YibaoAlias(value = "free3")
    private String freeThree;
}
