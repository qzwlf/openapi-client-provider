package com.acooly.module.openapi.client.provider.yinsheng.message.dto;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import java.io.Serializable;

/**
 * @author zhike 2018/1/9 10:19
 */
@Getter
@Setter
public class YinShengInfo implements Serializable{
    /**
     * 店铺时间YYYYMMdd(隐藏参数，传了就订单号前八位就不用强制传当前日期了)
     */
    @NotBlank
    private String shopdate;

    /**
     * 商户订单号
     * 银盛支付合作商户网站唯一订单号。该参数支持汉字，前8位必须为当前日期，最大长度为32
     */
    @NotBlank
    private String out_trade_no;

    /**
     * 商品描述
     * 商品的标题/交易标题/订单标题/订单关键字等。 该参数最长为250个汉字。
     */
    @NotBlank
    private String subject;

    /**
     * 订单总金额
     * 该笔订单的资金总额，单位为RMB-Yuan。取值范围为[0.01，100000000.00]，精确到小数点后两位。Number(10,2)指10位长度，2位精度
     */
    @NotBlank
    private String total_amount;

    /**
     * 币种
     * 支持币种：CNY（人民币）、HKD（港币）、USD（美元）、EUR（欧元）、JPY（日元）。注：不填默认是CNY（人民币），如是非跨境商户，币种不能填HKD（港币）、USD（美元）、EUR（欧元）、JPY（日元）中的任意一种外币币种。
     */
    private String currency;

    /**
     * 商户号
     * 收款方银盛支付用户号（商户号）
     */
    private String seller_id;

    /**
     * 商户名
     * 收款方银盛支付客户名（注册时公司名称）
     */
    @NotBlank
    private String seller_name;

    /**
     * 未付款时间
     * 设置未付款交易的超时时间，一旦超时，该笔交易就会自动被关闭。 取值范围：3d～15d。 m-分钟， h-小时， d-天。 该参数数值不接受小数点，如1.5h，可转换为90m。 如果小于3天，系统取3天为超时时间。
     */
    @NotBlank
    private String timeout_express;

    /**
     * 扩展参数 隐秘信息扩展参数，一个json字符串
     */
    private String extend_params;

    /**
     * 业务代码 	业务代码由银盛人员提供
     */
    @NotBlank
    private String business_code;

}
