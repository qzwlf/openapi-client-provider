package com.acooly.module.openapi.client.provider.yinsheng.message;


import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.yinsheng.domain.YinShengApiMsgInfo;
import com.acooly.module.openapi.client.provider.yinsheng.domain.YinShengResponse;
import com.acooly.module.openapi.client.provider.yinsheng.enums.YinShengServiceEnum;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@YinShengApiMsgInfo(service = YinShengServiceEnum.TRADE_ORDER_QUEERY, type = ApiMessageType.Response)
public class YinShengTradeOrderQueryResponse extends YinShengResponse {
    /**
     *订单号
     */
    private String out_trade_no;

    /**
     *银盛流水号
     */
    private String trade_no;

    /**
     *状态码
     */
    private String trade_status;

    /**
     *订单总金额
     */
    private String total_amount;

    /**
     *实收金额
     */
    private String receipt_amount;

    /**
     *日期
     */
    private String account_date;


}
