/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhike@acooly.cn 2017-09-22 09:22 创建
 */
package com.acooly.module.openapi.client.provider.shengpay.domain;

import com.acooly.core.utils.Dates;
import com.acooly.core.utils.ToString;
import com.acooly.module.openapi.client.api.message.ApiMessage;
import com.acooly.module.openapi.client.provider.shengpay.ShengpayConstants;
import com.alibaba.fastjson.annotation.JSONField;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Transient;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * 盛付通存管基础报文
 *
 * @author zhike 2017-09-22 09:22
 */
@Getter
@Setter
public class ShengpayMessage implements ApiMessage {

    /**
     * 采用RESTLET的URL路径作为服务唯一标志（服务名）
     */
    @JSONField(serialize = false)
    @NotEmpty
    @Length(max = 128)
    private String service;

    /**
     * 接入商户标志（商户号:merchantNo）
     */
    @NotEmpty
    private String merchantNo;

    /**
     * 字符集,支持GBK、UTF-8、GB2312,默认属性值为:UTF-8
     */
    private String charset = ShengpayConstants.SHENGPAY_CHARSET;

    /**
     * 扩展属性,JSON串
     */
    private String exts;

    /**
     * 签名类型，支持的签名方式为: RSA
     */
    private String signType = ShengpayConstants.SHENGPAY_SIGN_TYPE;

    /**
     * 签名消息
     */
    @Size(max = 512)
    private String signMsg;

    public String getSignStr() {
        return null;
    }

    public void doCheck() {

    }

    @Override
    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    @Override
    public String getPartner() {
        return merchantNo;
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
