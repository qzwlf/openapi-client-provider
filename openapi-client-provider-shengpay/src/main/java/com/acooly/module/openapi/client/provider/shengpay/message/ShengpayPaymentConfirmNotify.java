package com.acooly.module.openapi.client.provider.shengpay.message;

import com.acooly.core.common.exception.BusinessException;
import com.acooly.core.utils.Dates;
import com.acooly.core.utils.Strings;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.shengpay.ShengpayConstants;
import com.acooly.module.openapi.client.provider.shengpay.domain.ShengpayAlias;
import com.acooly.module.openapi.client.provider.shengpay.domain.ShengpayApiMsg;
import com.acooly.module.openapi.client.provider.shengpay.domain.ShengpayNotify;
import com.acooly.module.openapi.client.provider.shengpay.enums.ShengpayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Date;

/**
 * @author zhike 2018/5/17 18:59
 */
@Getter
@Setter
@ShengpayApiMsg(service = ShengpayServiceNameEnum.PAYMENT_CONFIRM,type = ApiMessageType.Notify)
public class ShengpayPaymentConfirmNotify extends ShengpayNotify {

    /**
     * 本名称,默认属性值为:REP_B2CPAYMENT
     */
    @ShengpayAlias("Name")
    private String name;

    /**
     * 版本号,默认属性值为: V4.4.1.1.1
     */
    @ShengpayAlias("Version")
    private String version;

    /**
     * 字符集,支持GBK、UTF-8、GB2312,默认属性值为:UTF-8
     */
    @ShengpayAlias("Charset")
    private String charset = ShengpayConstants.SHENGPAY_CHARSET;

    /**
     * 报文发起方唯一消息标识
     */
    @ShengpayAlias("TraceNo")
    private String traceNo;

    /**
     * 发送方标识，由盛付通提供,默认为:SFT
     */
    @ShengpayAlias("MsgSender")
    private String msgSender;

    /**
     * 商户外部会员号
     */
    @ShengpayAlias("outMemberId")
    private String outMemberId;

  /** 商户网站提交查询请求,为14位正整数数字,格式为:yyyyMMddHHmmss,如:20110707112233 */
  @ShengpayAlias("SendTime")
  private String sendTime = Dates.format(new Date(),"yyyyMMddHHmmss");

    /**
     * 支付机构号，比如：ICBC
     */
    @ShengpayAlias("InstCode")
    private String instCode;

    /**
     * 产品订单号
     */
    @ShengpayAlias("OrderNo")
    private String orderNo;

    /**
     * 订单金额
     */
    @ShengpayAlias("OrderAmount")
    private String orderAmount;

    /**
     * 产品订单号
     */
    @ShengpayAlias("TransNo")
    private String transNo;

    /**
     * 订单金额
     */
    @ShengpayAlias("TransAmount")
    private String transAmount;

    /**
     * TransStatus
     */
    @ShengpayAlias("TransStatus")
    private String transStatus;

    /**
     * 类型
     */
    @ShengpayAlias("TransType")
    private String transType;

    /**
     * 时间，为14位正整数数字,格式为:yyyyMMddHHmmss,如:20110707112233
     */
    @ShengpayAlias("TransTime")
    private String transTime;

    /**
     * 商户号
     */
    @ShengpayAlias("MerchantNo")
    private String merchantNo;

    /**
     * 错误码
     */
    @ShengpayAlias("ErrorCode")
    private String errorCode;

    /**
     * 错误信息
     */
    @ShengpayAlias("ErrorMsg")
    private String errorMsg;

    /**
     * 英文或中文字符串
     */
    @ShengpayAlias("Ext1")
    private String ext1;

    /**
     * 英文或中文字符串
     */
    @ShengpayAlias("Ext2")
    private String ext2;

    /**
     * 签名类型,支持类型：RSA
     */
    @ShengpayAlias("SignType")
    private String signType;

    /**
     * 签名结果
     */
    @ShengpayAlias("SignMsg")
    private String signMsg;
    /**
     *
     */
    @ShengpayAlias("paymentStatus")
    private String paymentStatus;
    /**
     *
     */
    @ShengpayAlias("resultType")
    private String resultType;

    public String getSignMsg(){
        String sign = null;
        try {
            sign = URLDecoder.decode(this.signMsg,"UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new BusinessException("URLDecoder.decode盛支付通知签名结果异常");
        }
        return sign;
    }
    @Override
    public String getSignStr(){
        StringBuilder signStr = new StringBuilder();
        if (Strings.isNotBlank(getName())) {
            signStr.append(getName()+"|");
        }
        if (Strings.isNotBlank(getVersion())) {
            signStr.append(getVersion()+"|");
        }
        if (Strings.isNotBlank(getCharset())) {
            signStr.append(getCharset()+"|");
        }
        if (Strings.isNotBlank(getTraceNo())) {
            signStr.append(getTraceNo()+"|");
        }
        if (Strings.isNotBlank(getMsgSender())) {
            signStr.append(getMsgSender()+"|");
        }
        if (Strings.isNotBlank(getOutMemberId())) {
            signStr.append(getOutMemberId()+"|");
        }
        if (Strings.isNotBlank(getSendTime())) {
            signStr.append(getSendTime()+"|");
        }
        if (Strings.isNotBlank(getInstCode())) {
            signStr.append(getInstCode()+"|");
        }
        if (Strings.isNotBlank(getOrderNo())) {
            signStr.append(getOrderNo()+"|");
        }
        if (Strings.isNotBlank(getOrderAmount())) {
            signStr.append(getOrderAmount()+"|");
        }
        if (Strings.isNotBlank(getTransNo())) {
            signStr.append(getTransNo()+"|");
        }
        if (Strings.isNotBlank(getTransAmount())) {
            signStr.append(getTransAmount()+"|");
        }
        if (Strings.isNotBlank(getTransStatus())) {
            signStr.append(getTransStatus()+"|");
        }
        if (Strings.isNotBlank(getTransType())) {
            signStr.append(getTransType()+"|");
        }
        if (Strings.isNotBlank(getTransTime())) {
            signStr.append(getTransTime()+"|");
        }
        if (Strings.isNotBlank(getMerchantNo())) {
            signStr.append(getMerchantNo()+"|");
        }
        if (Strings.isNotBlank(getPaymentStatus())) {
            signStr.append(getPaymentStatus()+"|");
        }
        if (Strings.isNotBlank(getResultType())) {
            signStr.append(getResultType()+"|");
        }
        if (Strings.isNotBlank(getErrorCode())) {
            signStr.append(getErrorCode()+"|");
        }
        if (Strings.isNotBlank(getErrorMsg())) {
            signStr.append(getErrorMsg()+"|");
        }
        if (Strings.isNotBlank(getExt1())) {
            signStr.append(getExt1()+"|");
        }
        if (Strings.isNotBlank(getExt2())) {
            signStr.append(getExt2()+"|");
        }
        if (Strings.isNotBlank(getSignType())) {
            signStr.append(getSignType()+"|");
        }
        try {
            return URLDecoder.decode(signStr.toString(),"UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new BusinessException("URLDecoder.decode盛支付通知原始结果异常");
        }
    }
}
