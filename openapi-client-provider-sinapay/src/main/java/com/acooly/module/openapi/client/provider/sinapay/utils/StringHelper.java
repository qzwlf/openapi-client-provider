package com.acooly.module.openapi.client.provider.sinapay.utils;

import com.alibaba.fastjson.JSONObject;

import java.util.Map;

/**
 * @author zhike 2018/4/18 11:00
 */
public class StringHelper {


    /**
     * 组装请求字符串
     * @param requestMap
     * @return
     */
    public static String getRequestStr(Map<String,String> requestMap){
        StringBuilder sb = new StringBuilder();
        for (Map.Entry entry : requestMap.entrySet()) {
            sb.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
        }
        if (sb.length() > 1) {
            sb.deleteCharAt(sb.length() - 1);
        }
        return sb.toString();
    }
    /**
     * 判断一个字符串是否是json格式
     * @param content
     * @return
     */
    public static boolean isJson(String content) {

        try {
            JSONObject jsonStr = JSONObject.parseObject(content);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
