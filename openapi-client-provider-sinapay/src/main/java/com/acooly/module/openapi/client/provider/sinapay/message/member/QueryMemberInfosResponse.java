package com.acooly.module.openapi.client.provider.sinapay.message.member;

import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayResponse;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * @author zhike 2018/7/10 15:03
 */
@Getter
@Setter
@SinapayApiMsg(service = SinapayServiceNameEnum.QUERY_MEMBER_INFOS, type = ApiMessageType.Response)
public class QueryMemberInfosResponse extends SinapayResponse {

    /**
     *会员类型
     * 会员类型，详见附录，默认企业，且只支持企业
     */
    @Size(max = 16)
    @ApiItem(value = "member_type")
    private String memberType;

    /**
     *公司名称
     * 公司名称，以便审核通过
     */
    @NotEmpty
    @Size(max = 90)
    @ApiItem(value = "company_name")
    private String companyName;

    /**
     *企业网址
     */
    @Size(max = 90)
    @ApiItem(value = "website")
    private String website;

    /**
     *企业地址
     */
    @NotEmpty
    @Size(max = 256)
    @ApiItem(value = "address")
    private String address;

    /**
     *执照号
     * 执照号，以便审核通过
     */
    @NotEmpty
    @Size(max = 50)
    @ApiItem(value = "license_no")
    private String licenseNo;

    /**
     *营业执照所在地
     */
    @NotEmpty
    @Size(max = 256)
    @ApiItem(value = "license_address")
    private String licenseAddress;

    /**
     *执照过期日（营业期限）
     * 格式为“YYYYMMDD”
     */
    @NotEmpty
    @Size(max = 32)
    @ApiItem(value = "license_expire_date")
    private String licenseExpireDate;

    /**
     *营业范围
     */
    @NotEmpty
    @Size(max = 50)
    @ApiItem(value = "business_scope")
    private String businessScope;

    /**
     *联系电话
     */
    @NotEmpty
    @Size(max = 20)
    @ApiItem(value = "telephone")
    private String telephone;

    /**
     *联系Email
     */
    @Size(max = 50)
    @ApiItem(value = "email")
    private String email;

    /**
     *组织机构代码
     */
    @NotEmpty
    @Size(max = 32)
    @ApiItem(value = "organization_no")
    private String organizationNo;


    /**
     *企业简介
     */
    @NotEmpty
    @Size(max = 512)
    @ApiItem(value = "summary")
    private String summary;

    /**
     *企业法人
     * 掩码信息
     */
    @NotEmpty
    @Size(max = 50)
    @ApiItem(value = "legal_person")
    private String legalPerson;

    /**
     *证件号码
     * 掩码信息
     */
    @NotEmpty
    @Size(max = 18)
    @ApiItem(value = "cert_no")
    private String certNo;

    /**
     *证件类型
     * 见附录，目前只支持身份证
     */
    @NotEmpty
    @Size(max = 18)
    @ApiItem(value = "cert_type")
    private String certType;


    /**
     *法人手机号码
     */
    @NotEmpty
    @Size(max = 18)
    @ApiItem(value = "legal_person_phone")
    private String legalPersonPhone;
}
