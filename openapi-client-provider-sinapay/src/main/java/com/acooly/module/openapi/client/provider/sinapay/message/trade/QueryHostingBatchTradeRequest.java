package com.acooly.module.openapi.client.provider.sinapay.message.trade;

import com.acooly.core.utils.Ids;
import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.sinapay.annotation.SinaGateWay;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayRequest;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayGateWayType;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

/**
 * 交易批次查询
 *
 * @author xiaohong
 * @create 2018-07-17 15:54
 **/
@Getter
@Setter
@SinaGateWay(SinapayGateWayType.PAYMENT)
@SinapayApiMsg(service = SinapayServiceNameEnum.QUERY_HOSTING_BATCH_TRADE, type = ApiMessageType.Request)
public class QueryHostingBatchTradeRequest extends SinapayRequest {
    /**
     * 交易批次号
     */
    @NotEmpty
    @Size(max = 32)
    @ApiItem(value = "out_batch_no")
    private String outBatchNo = Ids.oid();

    /**
     * 页号
     */
    @Min(1)
    @ApiItem(value = "page_no")
    private int pageNo = 1;

    /**
     * 每页大小
     */
    @Max(20)
    @ApiItem(value = "page_size")
    private int pageSize = 20;
}
