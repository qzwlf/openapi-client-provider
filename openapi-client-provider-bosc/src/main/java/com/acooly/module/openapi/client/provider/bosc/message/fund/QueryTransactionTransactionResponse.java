package com.acooly.module.openapi.client.provider.bosc.message.fund;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.bosc.domain.ApiMsgInfo;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscResponse;
import com.acooly.module.openapi.client.provider.bosc.enums.BoscServiceNameEnum;
import com.acooly.module.openapi.client.provider.bosc.message.fund.info.TransactionDetailInfo;

import java.util.List;

@ApiMsgInfo(service = BoscServiceNameEnum.QUERY_TRANSACTION_TRANSACTION ,type = ApiMessageType.Response)
public class QueryTransactionTransactionResponse extends BoscResponse {
	
	private List<TransactionDetailInfo> records;
	
	public List<TransactionDetailInfo> getRecords () {
		return records;
	}
	
	public void setRecords (
			List<TransactionDetailInfo> records) {
		this.records = records;
	}
}