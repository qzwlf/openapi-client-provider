package com.acooly.module.openapi.client.provider.bosc.message.trade;

import com.acooly.module.openapi.client.provider.bosc.domain.ApiMsgInfo;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.bosc.enums.BoscServiceNameEnum;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscRequest;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

@ApiMsgInfo(service = BoscServiceNameEnum.QUERY_PROJECT_INFORMATION, type = ApiMessageType.Request)
public class QueryProjectInformationRequest extends BoscRequest {
	/**
	 * 标的号
	 */
	@NotEmpty
	@Size(max = 50)
	private String projectNo;
	
	public QueryProjectInformationRequest () {
		setService (BoscServiceNameEnum.QUERY_PROJECT_INFORMATION.code ());
	}
	
	public QueryProjectInformationRequest (String projectNo) {
		this();
		this.projectNo = projectNo;
	}
	
	public String getProjectNo () {
		return projectNo;
	}
	
	public void setProjectNo (String projectNo) {
		this.projectNo = projectNo;
	}
}