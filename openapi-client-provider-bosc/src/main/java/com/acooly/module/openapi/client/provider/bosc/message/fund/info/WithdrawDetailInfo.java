/*
 *
 * www.cutebear.com Inc.
 * Copyright (c) 2017  All Rights Reserved
 */

package com.acooly.module.openapi.client.provider.bosc.message.fund.info;

import com.acooly.core.utils.Money;
import com.acooly.core.utils.validate.jsr303.MoneyConstraint;
import com.acooly.module.openapi.client.provider.bosc.enums.BoscStatusEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.fund.BoscBackrollStatusEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.fund.BoscRemitTypeEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.fund.BoscWithdrawFormEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.fund.BoscWithdrawStatusEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.fund.BoscWithdrawTypeEnum;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * Created by liubin@prosysoft.com on 2017/10/16.
 */
public class WithdrawDetailInfo {
	
	
	/**
	 * 提现金额
	 */
	@MoneyConstraint
	private Money amount;
	/**
	 * 提现分佣
	 */
	@MoneyConstraint
	private Money commission;
	/**
	 * 平台用户编号
	 */
	@NotEmpty
	@Size(max=50)
	private String platformUserNo;
	/**
	 * 交易发起时间
	 */
	@NotNull
	private Date createTime;
	/**
	 * 交易完成时间
	 */
	private Date transactionTime;
	/**
	 * 出款时间
	 */
	private Date remitTime;
	/**
	 * 到账时间
	 */
	private Date completedTime;
	/**
	 * 见【提现交易状态】
	 */
	@NotNull
	private BoscWithdrawStatusEnum status;
	/**
	 * 提现银行卡号（仅显示银行卡后四位）
	 */
	@NotEmpty
	@Size(max=50)
	private String bankcardNo;
	/**
	 * 提现类型，IMMEDIATE 为直接提现，CONFIRMED 为待确认提现，默认为直接提现 方式
	 */
	@NotNull
	private BoscWithdrawFormEnum withdrawForm;
	/**
	 * 提现失败的订单回充后会返回该状态：SUCCESS（已回退）、PENDDING（回退中）INIT（初始化）
	 */
	private BoscBackrollStatusEnum backRollStatus;
	/**
	 * 出款类型；NORMAL：T1 出款；NORMAL_URGENT：普通 T0 出款；URGENT： 实时 T0 出款；
	 */
	@NotNull
	private BoscRemitTypeEnum remitType;
	/**
	 * 见【提现方式】
	 */
	@NotNull
	private BoscWithdrawTypeEnum withdrawWay;
	/**
	 * 垫资金额
	 */
	@MoneyConstraint
	private Money floatAmount;
	
	public Money getAmount () {
		return amount;
	}
	
	public void setAmount (Money amount) {
		this.amount = amount;
	}
	
	public Money getCommission () {
		return commission;
	}
	
	public void setCommission (Money commission) {
		this.commission = commission;
	}
	
	public String getPlatformUserNo () {
		return platformUserNo;
	}
	
	public void setPlatformUserNo (String platformUserNo) {
		this.platformUserNo = platformUserNo;
	}
	
	public Date getCreateTime () {
		return createTime;
	}
	
	public void setCreateTime (Date createTime) {
		this.createTime = createTime;
	}
	
	public Date getTransactionTime () {
		return transactionTime;
	}
	
	public void setTransactionTime (Date transactionTime) {
		this.transactionTime = transactionTime;
	}
	
	public Date getRemitTime () {
		return remitTime;
	}
	
	public void setRemitTime (Date remitTime) {
		this.remitTime = remitTime;
	}
	
	public Date getCompletedTime () {
		return completedTime;
	}
	
	public void setCompletedTime (Date completedTime) {
		this.completedTime = completedTime;
	}
	
	public BoscWithdrawStatusEnum getStatus () {
		return status;
	}
	
	public void setStatus (BoscWithdrawStatusEnum status) {
		this.status = status;
	}
	
	public String getBankcardNo () {
		return bankcardNo;
	}
	
	public void setBankcardNo (String bankcardNo) {
		this.bankcardNo = bankcardNo;
	}
	
	public BoscWithdrawFormEnum getWithdrawForm () {
		return withdrawForm;
	}
	
	public void setWithdrawForm (BoscWithdrawFormEnum withdrawForm) {
		this.withdrawForm = withdrawForm;
	}
	
	public BoscBackrollStatusEnum getBackRollStatus () {
		return backRollStatus;
	}
	
	public void setBackRollStatus (
			BoscBackrollStatusEnum backRollStatus) {
		this.backRollStatus = backRollStatus;
	}
	
	public BoscRemitTypeEnum getRemitType () {
		return remitType;
	}
	
	public void setRemitType (BoscRemitTypeEnum remitType) {
		this.remitType = remitType;
	}
	
	public BoscWithdrawTypeEnum getWithdrawWay () {
		return withdrawWay;
	}
	
	public void setWithdrawWay (BoscWithdrawTypeEnum withdrawWay) {
		this.withdrawWay = withdrawWay;
	}
	
	public Money getFloatAmount () {
		return floatAmount;
	}
	
	public void setFloatAmount (Money floatAmount) {
		this.floatAmount = floatAmount;
	}
}
