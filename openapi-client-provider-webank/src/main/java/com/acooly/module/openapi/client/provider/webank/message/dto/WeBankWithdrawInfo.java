package com.acooly.module.openapi.client.provider.webank.message.dto;

import org.hibernate.validator.constraints.NotBlank;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

/**
 * 签约订单参数
 * @author fufeng
 */
@Getter
@Setter
public class WeBankWithdrawInfo implements Serializable {

    /**
     * 版本号：由微众分配，初始为2.0.0
     */
    protected String version="2.0.0";

    /**
     * 场景号
     */
    protected String scenarioNo;

    /**
     * 对公行号
     */
    protected String bankNo;

    /**
     * 商户号（16）
     */
    @NotBlank
    protected String merId;

    /**
     * 订单号（32）
     */
    protected String orderId;


    /**
     * 交易类型
     */
    protected String bizType= "01";

    /**
     * 请求日期 YYYYMMDD
     */
    protected String reqDate = new SimpleDateFormat("yyyyMMdd").format(new Date());

    /**
     * 请求时间 HHmmSS
     */
    protected String reqTime = new SimpleDateFormat("HHmmss").format(new Date());

    /**
     * 账号
     */
    protected String acctNo;

    /**
     * 户名
     */
    protected String acctName;

    /**
     * 币种 固定"01:人民币"
     */
    protected String ccy="01";

    /**
     * 交易金额
     */
    protected String amount;


    /**
     * 备注
     */
    protected String remark;

    /**
     * 用户证件类型
     */
    protected String idType;

    /**
     * 用户证件号码
     */
    protected String idNum;

    /**
     * 对公对私标识 11：对私，12：对公
     */
    protected String acctFlag;

    /**
     * 卡类型
     *  01：借记卡
     *  02：贷记卡
     */
    protected String cardType;


    /**
     * 信用卡有效期
     */
    protected String validDate;

    /**
     * CVN2
     */
    protected String CVN2;

    /**
     * 手机号
     */
    protected String phoneNo;

    /**
     * 交易结果通知URL
     */
    protected String notifyUrl;

}
