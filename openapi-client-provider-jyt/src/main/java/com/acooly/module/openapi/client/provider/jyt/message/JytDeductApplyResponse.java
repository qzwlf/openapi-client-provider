package com.acooly.module.openapi.client.provider.jyt.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.jyt.domain.JytApiMsgInfo;
import com.acooly.module.openapi.client.provider.jyt.domain.JytResponse;
import com.acooly.module.openapi.client.provider.jyt.enums.JytServiceEnum;
import com.acooly.module.openapi.client.provider.jyt.message.dto.JytDeductApplyResponseBody;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/5/7 23:37
 */
@Getter
@Setter
@JytApiMsgInfo(service = JytServiceEnum.DEDUCT_APPLY,type = ApiMessageType.Response)
@XStreamAlias("message")
public class JytDeductApplyResponse extends JytResponse {

    /**
     * 响应报文体
     */
    @XStreamAlias("body")
    private JytDeductApplyResponseBody deductApplyResponseBody;
}
