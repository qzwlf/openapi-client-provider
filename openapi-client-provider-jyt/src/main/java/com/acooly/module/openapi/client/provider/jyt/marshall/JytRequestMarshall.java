/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhangpu 
 * date:2016年4月1日
 *
 */
package com.acooly.module.openapi.client.provider.jyt.marshall;

import com.acooly.module.openapi.client.api.marshal.ApiMarshal;
import com.acooly.module.openapi.client.provider.jyt.domain.JytRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * @author zhangpu
 */
@Service
public class JytRequestMarshall extends JytMarshallSupport implements ApiMarshal<String, JytRequest> {

    private static final Logger logger = LoggerFactory.getLogger(JytRequestMarshall.class);

    @Override
    public String marshal(JytRequest source) {
        beforeMarshall(source);
        return doMarshall(source);
    }

}
