package com.acooly.module.openapi.client.provider.fuyou.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fuyou.domain.FuyouApiMsgInfo;
import com.acooly.module.openapi.client.provider.fuyou.domain.FuyouNotify;
import com.acooly.module.openapi.client.provider.fuyou.enums.FuyouServiceEnum;
import com.acooly.module.openapi.client.provider.fuyou.support.FuyouAlias;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/3/19 15:51
 */
@Getter
@Setter
@FuyouApiMsgInfo(service = FuyouServiceEnum.FIRST_QUICKPAY_CREATE_ORDER,type = ApiMessageType.Notify)
@XStreamAlias("RESPONSE")
public class FuyouFirstQuickCreateOrderNotify extends FuyouNotify{

    /**
     * 交易类型
     */
    @FuyouAlias("TYPE")
    private String type = "03";

    /**
     * 响应代码
     * 0000 成功
     */
    @FuyouAlias("RESPONSECODE")
    private String responseCode;

    /**
     * 响应中文描述
     */
    @FuyouAlias("RESPONSEMSG")
    private String responseMsg;

    /**
     * 商户订单号
     * 商户订单流水号商户确保唯一
     */
    @Size(max = 60)
    @NotBlank
    @FuyouAlias("MCHNTORDERID")
    private String merchOrderNo;

    /**
     * 富友订单号
     * 富友生成的订单号，该订单号在相
     当长的时间内不重复。富友通过订
     单号来唯一确认一笔订单的重复性
     */
    @FuyouAlias("ORDERID")
    private String bankOrderId;

    /**
     * 协议号
     */
    @FuyouAlias("PROTOCOLNO")
    private String protocolno;

    /**
     * 交易金额
     * 交易金额，分为单位
     */
    @FuyouAlias("AMT")
    @NotBlank
    @Size(max = 12)
    private String amount;

    /**
     * 签名类型
     */
    @FuyouAlias("SIGNTP")
    private String signType;

    /**
     * 银行卡号
     */
    @FuyouAlias("BANKCARD")
    private String bankCard;

    /**
     * 摘要数据
     */
    @FuyouAlias("SIGN")
    private String sign;

    /**
     * 验签串
     */
    private String signStr;

    @Override
    public String getSignStr() {
        return getType()+"|"+getVersion()+"|"+getResponseCode()+"|"+getPartner()+"|"+getMerchOrderNo()+"|"+getBankOrderId()+"|"+getAmount()+"|"+getBankCard()+"|";
    }
}
