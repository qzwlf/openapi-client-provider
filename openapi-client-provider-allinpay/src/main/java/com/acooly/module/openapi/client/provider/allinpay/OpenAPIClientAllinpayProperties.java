/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhike@acooly.cn 2017-09-16 16:11 创建
 */
package com.acooly.module.openapi.client.provider.allinpay;

import com.acooly.core.utils.Strings;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

import static com.acooly.module.openapi.client.provider.allinpay.OpenAPIClientAllinpayProperties.PREFIX;

/**
 * @author zhike@acooly.cn
 */
@Getter
@Setter
@ConfigurationProperties(prefix = PREFIX)
public class OpenAPIClientAllinpayProperties {
    public static final String PREFIX = "acooly.openapi.client.allinpay";

    /**
     * 网关地址
     */
    private String gatewayUrl;

    /**
     * 公钥文件路径
     */
    private String publicKeyPath;

    /**
     * 私钥文件路径
     */
    private String privateKeyPath;

    /**
     * 私钥文件密码
     */
    private String privateKeyPassword;

    /**
     * 用于通联通收银宝
     */
    private String md5Key;

    /**
     * 本系统域名（用于自动生成回调地址）
     */
    private String domain;

    /**
     * 连接超时时间（毫秒）
     */
    private long connTimeout = 10000;

    /**
     * 读超时时间（毫秒）
     */
    private long readTimeout = 30000;

    /**
     * 商户号
     */
    private String partnerId;

    /**
     * 商户名
     */
    private String partnerName;

    /**
     * 商户密码
     */
    private String partnerPassword;

    /**
     * 对账文件存放路径
     */
    private String filePath;

    protected String getCanonicalUrl(String prefix, String postfix) {
        if (Strings.endsWith(prefix, "/")) {
            prefix = Strings.removeEnd(prefix, "/");
        }
        if (Strings.startsWith(postfix, "/")) {
            return prefix + postfix;
        } else {
            return prefix + "/" + postfix;
        }
    }

    private Checkfile checkfile = new Checkfile();

    @Getter
    @Setter
    public static class Checkfile {

        private String host;
        private int port;
        private String username;
        private String password;

        private String serverRoot;
        private String localRoot;

    }

}
