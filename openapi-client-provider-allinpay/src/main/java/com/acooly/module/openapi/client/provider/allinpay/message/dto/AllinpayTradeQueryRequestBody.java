package com.acooly.module.openapi.client.provider.allinpay.message.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @Auther: zhike
 * @Date: 2018/8/29 14:02
 * @Description:
 */
@Getter
@Setter
@ToString
public class AllinpayTradeQueryRequestBody implements Serializable {
    /**
     * 商户代码(通联接口设计问题不需要外出传，组件吃掉)
     */
    @Size(max = 15)
    @NotBlank
    @XStreamAlias("MERCHANT_ID")
    private String merchantId;

    /**
     * 要查询的交易流水
     * 也就是原请求交易中的REQ_SN的值
     */
    @NotBlank
    @Size(max = 40)
    @XStreamAlias("QUERY_SN")
    private String querySn;
}
