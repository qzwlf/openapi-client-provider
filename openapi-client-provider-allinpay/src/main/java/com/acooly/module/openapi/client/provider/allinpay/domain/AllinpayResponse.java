/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年3月31日
 *
 */
package com.acooly.module.openapi.client.provider.allinpay.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

/**
 * Fuiou 响应报文基类
 *
 * @author zhike
 */
@Getter
@Setter
public class AllinpayResponse extends AllinpayApiMessage {

	/**
	 * 响应报文头信息
	 */
	@XStreamAlias("INFO")
	private AllinpayResponsetInfo responsetInfo;
}
