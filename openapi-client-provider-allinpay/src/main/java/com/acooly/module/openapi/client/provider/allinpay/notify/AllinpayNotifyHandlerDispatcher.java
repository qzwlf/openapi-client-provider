/**
 * coding by zhike
 */
package com.acooly.module.openapi.client.provider.allinpay.notify;

import com.acooly.core.utils.Strings;
import com.acooly.module.openapi.client.api.ApiServiceClient;
import com.acooly.module.openapi.client.api.notify.AbstractSpringNotifyHandlerDispatcher;
import com.acooly.module.openapi.client.provider.allinpay.AllinpayApiServiceClient;
import com.acooly.module.openapi.client.provider.allinpay.AllinpayConstants;
import com.acooly.module.openapi.client.provider.allinpay.OpenAPIClientAllinpayProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 富友 支付网关异步通知分发器
 *
 * @author liuyuxiang
 * @date 2016年5月12日
 */
@Component
public class AllinpayNotifyHandlerDispatcher extends AbstractSpringNotifyHandlerDispatcher {

    @Autowired
    private OpenAPIClientAllinpayProperties openAPIClientAllinpayProperties;

    @Resource(name = "allinpayApiServiceClient")
    private AllinpayApiServiceClient apiServiceClient;

    @Override
    protected String getServiceKey(String notifyUrl, Map<String, String> notifyData) {
        return getServiceName(notifyUrl);
    }

    @Override
    protected ApiServiceClient getApiServiceClient() {
        return apiServiceClient;
    }

    private String getServiceName(String notifyUrl) {
        String canonicalUrl = AllinpayConstants.getCanonicalUrl("/",
                AllinpayConstants.FIX_NOTIFY_URL);
        String serviceName = Strings.substringAfter(notifyUrl, canonicalUrl);
        return serviceName;
    }
}
