/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-11-15 22:56 创建
 */
package com.acooly.openapi.client.provider.bosc.testjson;

import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

/**
 * @author zhangpu 2017-11-15 22:56
 */
@Getter
@Setter
@ToString
public class Group {

    private Long id;

    private String name;

    private List<User> users = Lists.newArrayList();

    public Group(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Group() {
    }
}
