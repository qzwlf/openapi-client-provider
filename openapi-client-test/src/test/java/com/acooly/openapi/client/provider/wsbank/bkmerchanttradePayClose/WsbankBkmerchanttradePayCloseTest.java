package com.acooly.openapi.client.provider.wsbank.bkmerchanttradePayClose;

import com.acooly.core.common.BootApp;
import com.acooly.module.openapi.client.provider.wsbank.WsbankApiService;
import com.acooly.module.openapi.client.provider.wsbank.message.WsbankBkmerchanttradePayCloseRequest;
import com.acooly.module.openapi.client.provider.wsbank.message.WsbankBkmerchanttradePayCloseResponse;
import com.acooly.module.openapi.client.provider.wsbank.message.dto.WsbankBkmerchanttradePayCloseRequestBody;
import com.acooly.module.openapi.client.provider.wsbank.message.dto.WsbankBkmerchanttradePayCloseRequestInfo;
import com.acooly.test.NoWebTestBase;
import com.alibaba.fastjson.JSON;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@BootApp(sysName = "wsbankBkmerchanttradePayCloseTest")
public class WsbankBkmerchanttradePayCloseTest extends NoWebTestBase {
    @Autowired
    private WsbankApiService wsbankApiService;
    
    /**
     * 测试订单关闭接口
     */
    @Test
    public void bkmerchanttradePayClose() {
    	WsbankBkmerchanttradePayCloseRequestBody requestBody = new WsbankBkmerchanttradePayCloseRequestBody();
    	requestBody.setMerchantId("226801000000103460124");
    	requestBody.setOutTradeNo("o18060113354409960003");
        WsbankBkmerchanttradePayCloseRequestInfo requestInfo = new WsbankBkmerchanttradePayCloseRequestInfo();
        requestInfo.setRequestBody(requestBody);
        WsbankBkmerchanttradePayCloseRequest request = new WsbankBkmerchanttradePayCloseRequest();
        request.setRequestInfo(requestInfo);
        WsbankBkmerchanttradePayCloseResponse response = wsbankApiService.bkmerchanttradePayClose(request);
        System.out.println("测试订单关闭接口："+ JSON.toJSONString(response));
    }
}
