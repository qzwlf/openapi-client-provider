package com.acooly.module.openapi.client.provider.wsbank.message.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author zhike 2018/5/24 14:51
 */
@Getter
@Setter
@XStreamAlias("body")
public class WsbankVostroNotifyBody implements Serializable {
    /**
     * 商户号。网商为商户分配的商户号，通过商户入驻结果查询接口获取。
     */
    @XStreamAlias("MerchantId")
    private String merchantId;

    /**
     * 合作方机构号（网商银行分配）
     */
    @XStreamAlias("IsvOrgId")
    private String isvOrgId;

    /**
     * 原外部订单请求流水号
     */
    @XStreamAlias("OutTradeNo")
    private String outTradeNo;

    /**
     * 原网商订单号
     */
    @XStreamAlias("OrderNo")
    private String orderNo;

    /**
     * 入账流水号
     */
    @XStreamAlias("TransNo")
    private String transNo;

    /**
     * 出款渠道id
     * （9001大额、9002小额、9003超网、9100支付宝）
     */
    @XStreamAlias("ChannelId")
    private String channelId;

    /**
     * 付款方银行联行号
     */
    @XStreamAlias("PayerBankOrgId")
    private String payerBankOrgId;

    /**
     *付款方卡号
     */
    @XStreamAlias("PayerCardNo")
    private String payerCardNo;

    /**
     * 付款方卡号户名
     */
    @XStreamAlias("PayerCardName")
    private String payerCardName;

    /**
     * 订单金额(金额为分)
     */
    @XStreamAlias("TotalAmount")
    private String totalAmount;

    /**
     * 币种，默认CNY
     */
    @XStreamAlias("Currency")
    private String currency;

    /**
     * 转账时间
     */
    @XStreamAlias("TransferDate")
    private String transferDate;

    /**
     * 备注
     */
    @XStreamAlias("Memo")
    private String memo;

    /**
     * 扩展信息
     */
    @XStreamAlias("ExtInfo")
    private String extInfo;

    /**
     * 订单状态
     * SUCCESS,FAIL
     */
    @XStreamAlias("Status")
    private String status;
}
