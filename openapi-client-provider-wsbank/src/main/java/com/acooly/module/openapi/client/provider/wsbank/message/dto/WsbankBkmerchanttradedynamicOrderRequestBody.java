package com.acooly.module.openapi.client.provider.wsbank.message.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author sunjx 2018/5/23 14:00
 */
@Getter
@Setter
@XStreamAlias("body")
public class WsbankBkmerchanttradedynamicOrderRequestBody implements Serializable {

	private static final long serialVersionUID = -4957566653955247068L;

	/**
	 * 外部交易号
	 */
	@Size(max = 64)
	@XStreamAlias("OutTradeNo")
	@NotBlank
	private String outTradeNo;

	/**
	 * 商品ID
	 */
	@Size(max = 32)
	@XStreamAlias("Goodsid")
	@NotBlank
	private String goodsid;

	/**
	 * 商品描述
	 */
	@Size(max = 128)
	@XStreamAlias("Body")
	@NotBlank
	private String body;

	/**
	 * 商品详情列表
	 */
	@XStreamAlias("GoodsDetail")
	private String goodsDetail;

	/**
	 * 交易总额度
	 */
	@XStreamAlias("TotalAmount")
	@NotBlank
	private String totalAmount;

	/**
	 * 币种
	 */
	@XStreamAlias("Currency")
	@NotBlank
	private String currency = "CNY";

	/**
	 * 商户号
	 */
	@Size(max = 64)
	@XStreamAlias("MerchantId")
	@NotBlank
	private String merchantId;

	/**
	 * 合作方机构号
	 */
	@Size(max = 64)
	@XStreamAlias("IsvOrgId")
	@NotBlank
	private String isvOrgId;

	/**
	 * 支付渠道类型。该笔支付走的第三方支付渠道。可选值： WX：微信支付 ALI：支付宝 QQ：手机QQ（暂未开放） JD：京东钱包（暂未开放）
	 */
	@Size(max = 64)
	@XStreamAlias("ChannelType")
	@NotBlank
	private String channelType;

	/**
	 * 操作员ID
	 */
	@Size(max = 32)
	@XStreamAlias("OperatorId")
	private String operatorId;

	/**
	 * 门店ID
	 */
	@Size(max = 32)
	@XStreamAlias("StoreId")
	private String storeId;

	/**
	 * 终端设备号
	 */
	@Size(max = 32)
	@XStreamAlias("DeviceId")
	private String deviceId;

	/**
	 * 终端IP
	 */
	@Size(max = 16)
	@XStreamAlias("DeviceCreateIp")
	@NotBlank
	private String deviceCreateIp;

	/**
	 * 订单有效期
	 */
	@Size(max = 60)
	@XStreamAlias("ExpireExpress")
	private String expireExpress;

	/**
	 * 清算方式 T0：T+0清算按笔清算，目前仅之前清算到余利宝，不支持清算到银行卡。 T1：T+1汇总清算，可支持清算到余利宝及清算到银行卡。
	 */
	@Size(max = 32)
	@XStreamAlias("SettleType")
	@NotBlank
	private String settleType = "T1";

	/**
	 * 附加信息
	 */
	@Size(max = 128)
	@XStreamAlias("Attach")
	private String attach;

	/**
	 * 禁用支付方式
	 */
	@Size(max = 32)
	@XStreamAlias("PayLimit")
	private String payLimit;

	/**
	 * 可打折金额
	 */
	@XStreamAlias("DiscountableAmount")
	private String discountableAmount;

	/**
	 * 不可打折金额
	 */
	@XStreamAlias("UndiscountableAmount")
	private String undiscountableAmount;

	/**
	 * 支付宝的店铺编号
	 */
	@Size(max = 32)
	@XStreamAlias("AlipayStoreId")
	private String alipayStoreId;

	/**
	 * 支持系统商返佣
	 */
	@Size(max = 64)
	@XStreamAlias("SysServiceProviderId")
	private String sysServiceProviderId;

	/**
	 * 花呗交易分期数
	 */
	@XStreamAlias("CheckLaterNm")
	private String checkLaterNm;

}
