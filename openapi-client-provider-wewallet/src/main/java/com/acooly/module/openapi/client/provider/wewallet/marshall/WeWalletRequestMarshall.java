/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhangpu 
 * date:2016年4月1日
 *
 */
package com.acooly.module.openapi.client.provider.wewallet.marshall;

import com.acooly.module.openapi.client.api.marshal.ApiMarshal;
import com.acooly.module.openapi.client.provider.wewallet.domain.WeWalletRequest;

import org.springframework.stereotype.Service;

/**
 * @author fufeng
 */
@Service
public class WeWalletRequestMarshall extends WeWalletMarshallSupport implements ApiMarshal<String, WeWalletRequest> {

    @Override
    public String marshal(WeWalletRequest source) {
        beforeMarshall(source);
        return doMarshall(source);
    }

}
